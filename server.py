from concurrent import futures
from email import message
import logging
import asyncio

from api.model import user, token
from api import repository, service
import grpc
import oauth_pb2
import oauth_pb2_grpc
import helloworld_pb2
import helloworld_pb2_grpc

# Logging setup
import logging
from log4mongo.handlers import BufferedMongoHandler

logging.basicConfig(level=logging.INFO)
handler = BufferedMongoHandler(
    host=repository.MONGODB_URL, database_name='logging-service', collection='logs')
logger = logging.getLogger('oauth-server')
logger.addHandler(handler)
logger.info('starting grpc server')

# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []

OAUTH_GRPC_PORT = 50052


class Greeter(helloworld_pb2_grpc.GreeterServicer):

    def SayHello(self, request, context):
        logger.info('say hello')
        return helloworld_pb2.HelloReply(message='Hello, %s!' % request.name)

    def SayHelloAgain(self, request, context):
        return helloworld_pb2.HelloReply(message='Hello again, %s!' % request.name)


class Oauth(oauth_pb2_grpc.OAuthServicer):
    async def Register(self, request, context):
        try:
            if request:
                logger.info(
                    f'Register request received, request: {request.username}, {request.role}, {type(request)}')
                if request.role == 'customer' or request.role == 'chef':
                    request_user = user.User(
                        username=request.username, password=request.password, role=request.role)
                else:
                    logger.info('Role invalid')
                    return oauth_pb2.RegisterReply(
                        status=400,
                        message="role is not customer or chef"
                    )
                res = await service.register_user(request_user)
                return oauth_pb2.RegisterReply(
                    status=res['status'],
                    message=res['message']
                )
            else:
                logger.warning('Request data not received')
                return oauth_pb2.RegisterReply(
                    status=500,
                    message="internal server error"
                )
        except Exception as e:
            logger.error('Exception: ' + repr(e))

    async def Login(self, request, context):
        try:
            if request:
                logger.info(
                    f'Login request received, username: {request.username}')
                request_user = user.Login(
                    username=request.username,
                    password=request.password,
                    scope=""
                )
                res = await service.get_user_token(request_user)
                logger.info(f"Login response: {str(res)}")
                return oauth_pb2.LoginReply(
                    status=res['status'],
                    message=res['message'],
                    accessToken=res['accessToken'],
                    expiresIn=res['expiresIn'],
                    tokenType=res['tokenType'],
                    scope=res['scope']
                )
            else:
                logger.warning('Request data not received')
                return oauth_pb2.RegisterReply(
                    status=500,
                    message="internal server error",
                    accessToken=None,
                    expiresIn=None,
                    tokenType=None,
                    scope=None
                )
        except Exception as e:
            logger.error('Exception: ' + repr(e))

    async def CurrentUser(self, request, context):
        try:
            if request:
                logger.info(f'Current user request received, token:{request}')
                res = await service.get_current_active_user_v2(request.accessToken)
                logger.info(f"Current user response: {str(res)}")
                return oauth_pb2.CurrentUserReply(
                    status=res['status'],
                    message=res['message'],
                    username=res['username'],
                    role=res['role']
                )
            else:
                logger.warning('Request data not received')
                return oauth_pb2.RegisterReply(
                    status=500,
                    message="internal server error",
                    username=None,
                    role=None,
                )
        except Exception as e:
            logger.error('Exception: ' + repr(e))

    async def Logout(self, request, context):
        try:
            logger.info(f"LOGOUT {request.accessToken}")
            if request:
                res = await service.get_current_active_user_v2(request.accessToken)
                if res['message'] == 'token is valid':
                    res = await service.delete_user_token(request.accessToken)
                    logger.info(f"Logout response: {str(res)}")
                    return oauth_pb2.LogoutReply(
                        status=204,
                        message="success"
                    )
                else:
                    logger.info(f"Logout response: {str(res)}")
                    return oauth_pb2.LogoutReply(
                        status=res['status'],
                        message=res['message']
                    )
            else:
                logger.warning('Request data not received')
                return oauth_pb2.RegisterReply(
                    status=500,
                    message="internal server error",
                )
        except Exception as e:
            logger.error('Exception: ' + repr(e))


async def serve():
    try:
        server = grpc.aio.server()
        oauth_pb2_grpc.add_OAuthServicer_to_server(Oauth(), server)
        helloworld_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
        server.add_insecure_port(f'[::]:{OAUTH_GRPC_PORT}')
        await server.start()
    except Exception as e:
        logger.error('Failed starting server: ' + repr(e))

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 0 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
