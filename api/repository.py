import motor.motor_asyncio
import certifi
import logging
from typing import Optional
from jose import JWTError, jwt
from pydantic import ValidationError
from datetime import datetime, timedelta
from passlib.context import CryptContext
from fastapi.encoders import jsonable_encoder
from fastapi import Depends, HTTPException, status
from fastapi.security import (
    OAuth2PasswordBearer,
    SecurityScopes
)

from api.model import user, token

import logging
from log4mongo.handlers import BufferedMongoHandler


MONGODB_URL = 'mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
DB_NAME = 'service-oauth'
ALGORITHM = 'HS256'
SECRET_KEY = '43fa04547296eefd9740ac4937824bf293997e75da016ee2040ccbf482f23616'
ACCESS_TOKEN_EXPIRE_MINUTES = 600

ca = certifi.where()
client = motor.motor_asyncio.AsyncIOMotorClient(
    MONGODB_URL, serverSelectionTimeoutMS=5000, tlsCAFile=ca)

logging.basicConfig(level=logging.INFO)
handler = BufferedMongoHandler(
    host=MONGODB_URL, database_name='logging-service', collection='logs')
logger = logging.getLogger('oauth-server')
logger.addHandler(handler)

try:
    db = client[DB_NAME]
except Exception:
    logger.error("Unable to connect to MongoDB")

userCollection = db['user']
tokenCollection = db['token']

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='api/login',
    scopes={
        'me': 'Read current user profile',
    }
)
pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')


async def is_field_exist(username: Optional[str] = None):
    if username:
        user = await userCollection.find_one({'username': username})
        return user


async def insert_item(user: Optional[user.User] = None, token: Optional[token.Token] = None):
    if user:
        user['password'] = get_password_hash(user['password'])
        return await userCollection.insert_one(user)
    logger.info("INSERTING TOKEN {} {}".format(user, token))
    if token:
        return await tokenCollection.insert_one(token)


async def get_specific_item(username: Optional[str] = None, token: Optional[str] = None):
    logger.info(f"Get {username}{token}")
    if username:
        return await userCollection.find_one({'username': username})
    if token:
        return await tokenCollection.find_one({'access_token': token})


async def delete_user_token(token: str):
    logger.info(f"Delete {token}")
    if token:
        return await tokenCollection.delete_one({'access_token': token})


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


async def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=60)
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f'Bearer'

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
        headers={'WWW-Authenticate': authenticate_value}
    )

    try:
        logging.info('USER_REPOSITORY', 'get_current_user token input', token)
        token_dict = await get_specific_item(token=token)
        logging.info('USER_REPOSITORY',
                     'get_current_user token from db', token_dict)
        if token_dict is None:
            raise credentials_exception
        token = token_dict['access_token']
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        logging.info('USER_REPOSITORY', 'get_current_user', payload)
        username: str = payload.get('username')
        if username is None:
            raise credentials_exception
        token_scopes = payload.get('scopes', [])
    except (JWTError, ValidationError):
        raise credentials_exception

    user = await get_specific_item(username=username)
    if user is None:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_scopes['scopes']:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Not enough permissions',
                headers={'WWW-Authenticate': authenticate_value}
            )
    return {
        'user': user, 'token': token
    }


async def get_current_user_v2(token: str):
    credentials_exception = {
        'status': status.HTTP_401_UNAUTHORIZED,
        'message': 'Could not validate credentials',
        'username': None,
        'role': None
    }

    try:
        token_dict = await get_specific_item(token=token)
        if token_dict is None:
            logger.info("get_current_user_v2: no token in db")
            return credentials_exception
        token = token_dict['access_token']
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get('username')
        if username is None:
            await delete_user_token(token)
            logger.info("get_current_user_v2: no username")
            return credentials_exception
        elif datetime.utcnow() > datetime.fromtimestamp(payload.get('exp')):
            logger.info('get_current_user_v2: token expired')
            await delete_user_token(token)
            return credentials_exception

    except (JWTError, ValidationError):
        logger.info("get_current_user_v2: token invalid")
        return credentials_exception

    user = await get_specific_item(username=username)
    if user is None:
        logger.info("get_current_user_v2: user not in db")
        return credentials_exception
    return {
        'status': status.HTTP_200_OK,
        'message': 'token is valid',
        'username': user['username'],
        'role': user['role']
    }
