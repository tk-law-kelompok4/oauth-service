import logging
from datetime import datetime, timedelta
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, HTTPException, Security, status

from api.model import user
from api.model.token import Token
from api.model.tokenData import TokenData
from api import repository

from log4mongo.handlers import BufferedMongoHandler

logging.basicConfig(level=logging.DEBUG)
handler = BufferedMongoHandler(
    host=repository.MONGODB_URL, database_name='logging-service', collection='logs')
logger = logging.getLogger('oauth-server')
logger.addHandler(handler)


async def register_user(user: user.User):
    status_code = ''
    content = ''
    user = jsonable_encoder(user)
    check_is_field_valid = await repository.is_field_exist(
        username=user['username'],
    )
    if check_is_field_valid is None:
        insert_user = await repository.insert_item(user=user)
        if insert_user.acknowledged:
            logger.info(f"Inserted user {user['username']}")
            status_code = status.HTTP_201_CREATED
            content = {
                'status': status_code,
                'message': 'user successfully created',
            }
        else:
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            content = {
                'status': status_code,
                'message': 'internal server error',
            }
            logger.error(f"Failed to insert user {user['username']}")
    else:
        status_code = status.HTTP_403_FORBIDDEN
        content = {
            'status': status_code,
            'message': f"username {user['username']} alredy exists"
        }

    return content


async def get_user_token(login_data: user.Login):
    user = await repository.get_specific_item(username=login_data.username)
    if user is None:
        return {'status': status.HTTP_401_UNAUTHORIZED,
                'message': f"username {login_data.username} does not exist",
                'accessToken': None,
                'expiresIn': None,
                'tokenType': None,
                'scope': None
                }

    verify_password = repository.verify_password(
        login_data.password, user['password'])

    if not verify_password:
        return {
            'status': status.HTTP_401_UNAUTHORIZED,
            'message': f"incorrect username or password",
            'accessToken': None,
            'expiresIn': None,
            'tokenType': None,
            'scope': None
        }

    access_token_expires = timedelta(
        minutes=repository.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = await repository.create_access_token(
        data={'username': user['username'], 'role': user['role'],
              'scopes': login_data.scopes},
        expires_delta=access_token_expires
    )

    token = Token(
        access_token=str(access_token),
        token_type='bearer'
    )
    print("TOKEN OBJECT", token)
    print("TOKEN OBJECT JSON", jsonable_encoder(token))
    await repository.insert_item(token=jsonable_encoder(token))

    return {
        'status': status.HTTP_200_OK,
        'message': 'login successfull',
        'accessToken': access_token,
        'expiresIn': repository.ACCESS_TOKEN_EXPIRE_MINUTES,
        'tokenType': 'bearer',
        # kalo mau implement scope ganti jadi login_data.scopes
        'scope': None
    }


async def delete_user_token(token: str):
    return await repository.delete_user_token(token)


async def get_current_active_user(current_user=Security(repository.get_current_user)):
    return current_user


async def get_current_active_user_v2(token: str):
    current_user = await repository.get_current_user_v2(token)
    return current_user
