from fastapi import APIRouter, Depends, Security
from fastapi.security import OAuth2PasswordRequestForm

from api.model import user, token
from api import user as service

router = APIRouter(
    prefix='/api',
    tags=['auth'],
)


@router.post('/register')
async def register_user(user: user.User):
    return await service.register_user(user)


@router.post('/login')
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    return await service.get_user_token(form_data)

# @router.post('/login')
# async def login(user: user.Login):
#     return await service.get_user_token(user)


@router.get('/logout')
async def logout(current_user: user.User = Security(service.get_current_active_user)):
    user_token = await service.delete_user_token(current_user['token'])
    result = {
        'accessToken': False,
        'tokenType': None,
        'status': user_token.acknowledged
    }
    return result

#TODO: get_current_user


@router.get('/current-user')
async def get_current_user(current_user=Security(service.get_current_active_user)):
    print('USER_CONTROLLER', current_user)
    return {
        'username': current_user['user']['username'],
        'role': current_user['user']['role']
    }
