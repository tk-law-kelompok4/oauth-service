from datetime import datetime
from enum import Enum
from bson.objectid import ObjectId

from fastapi import File, UploadFile
from api.model.base_model import PyObjectId
from pydantic import BaseModel, EmailStr, Field


class Role(str, Enum):
    customer = 'customer'
    chef = 'chef'


class User(BaseModel):
    username: str = Field(..., max_length=50)
    password: str = Field(...)
    role: Role = Field(...)


class Login(BaseModel):
    username: str = Field(..., max_length=50)
    password: str = Field(...)
    scopes: list[str] = Field([])
